<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>View track</title>
<link rel="stylesheet" href="stylesheets/main.css" />
</head>
<body>
	<div id="image">
		<img width="200" alt="CoverImage"
			src="data:image/jpeg;base64,${cover}" />
	</div>
	<div id="text">
		<table>
			<tr>
				<td>ID:</td>
				<td>${id}</td>
			</tr>
			<tr>
				<td>Artist:</td>
				<td>${artist}</td>
			</tr>
			<tr>
				<td>Title:</td>
				<td>${title}</td>
			</tr>
			<tr>
				<td>Album:</td>
				<td>${album}</td>
			</tr>
		</table>
	</div>
</body>
</html>