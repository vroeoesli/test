package ch.vrdesign.smp.Worktest;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class Servlet extends HttpServlet {

	private static final long serialVersionUID = -6424760136781592088L;

	/*
	 * Creates a Track object from json-named textarea input from index.html
	 * 
	 * @see
	 * javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest
	 * , javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			JSONObject obj = new JSONObject(request.getParameter("json")
					.toString());

			Track track = new Track();

			track.setId(obj.getString("id"));
			track.setArtist(obj.getString("artist"));
			track.setTitle(obj.getString("title"));
			track.setAlbum(obj.getString("album"));
			track.setCover(obj.getString("cover"));

			request.setAttribute("id", track.getId());
			request.setAttribute("artist", track.getArtist());
			request.setAttribute("title", track.getTitle());
			request.setAttribute("album", track.getAlbum());
			request.setAttribute("cover", track.getCover());
			request.getRequestDispatcher("/response.jsp").forward(request,
					response);
		} catch (JSONException e) {
			System.err.println(String.format("Couldn't create JSON object. %s",
					e.getMessage()));
			e.printStackTrace();
		}
	}

}
