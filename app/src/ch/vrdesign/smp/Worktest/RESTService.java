package ch.vrdesign.smp.Worktest;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

@Path("/")
public class RESTService {
	@POST
	@Path("/service")
	@Consumes(MediaType.APPLICATION_JSON)
	/*
	 * Creates a Track object from parsed json input.
	 */
	public Response RESTResponse(InputStream incomingData) throws JSONException {

		StringBuilder restBuilder = new StringBuilder();

		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(
					incomingData));
			String line = null;
			while ((line = in.readLine()) != null) {
				restBuilder.append(line);
			}
		} catch (Exception e) {
			System.out.println(String.format("Error parsing: %s",
					e.getMessage()));
		}

		JSONObject obj = new JSONObject(restBuilder.toString());
		Track track = new Track();

		track.setId(obj.getString("id"));
		track.setArtist(obj.getString("artist"));
		track.setTitle(obj.getString("title"));
		track.setAlbum(obj.getString("album"));
		track.setCover(obj.getString("cover"));

		System.out.println(restBuilder.toString());

		return Response.status(200).entity(restBuilder.toString()).build();
	}
}