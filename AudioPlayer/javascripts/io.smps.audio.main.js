/* Copyright 2014 SwissMediaPartners AG, Mathieu Habegger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

window.AudioContext = window.AudioContext || window.webkitAudioContext;

var track = null;
var sourceNode = null;
var sourceStartTime = 0.0;

var totalDuration = 0.0;

var audioContext = new AudioContext();
var zeroGain = null;
var frameID = null;
var nowOffset = 0;
var framePID = null;
var analyserContext = null;
var trackCanvas = null;
var canvasWidth, canvasHeight;



function redraw(e) {
    var canvas = document.getElementById( "track" );
    totalDuration = track.duration;
    drawAudioBufferPart(canvas.width, canvas.height, canvas.getContext('2d'), track, 0, totalDuration, 0, 0, 0, "#54AB87");
}

function playTrack(e) {
    if (e.classList.contains("playing")) {
        sourceNode.stop(0);
        e.classList.remove("playing");
        sourceStartTime = 0.0;
    } else {
        e.classList.add("playing");
        sourceNode = audioContext.createBufferSource();
        sourceNode.buffer = track;
        sourceNode.connect( analyserNode );
        sourceNode.connect( audioContext.destination );
        sourceStartTime = audioContext.currentTime - nowOffset;
        sourceNode.start(0, nowOffset);
    }
}

function stopTrack(e) {
    nowOffset = 0;
    sourceStartTime = 0.0;

    var playBtn = document.getElementById( "playBtn" );
    if (playBtn.classList.contains("playing")) {
        sourceNode.stop(0);
        playBtn.classList.remove("playing");
    }
}

function loadAudioTrack(url) {

	this.buffer = null;
	this.url = url;

	if (!url)
		return;

	var request = new XMLHttpRequest();
	request.open("GET", url, true);
	request.responseType = "arraybuffer";
	request.onload = function() {
	  audioContext.decodeAudioData( request.response, function(buffer) {
	    	postLoadTask(buffer);
		} );
	}
	request.send();
}

function postLoadTask(inTrack) {
    track = inTrack;
    var canvas = null;
    canvas = document.getElementById("track");
    drawAudioBufferPart(canvas.width, canvas.height, canvas.getContext('2d'), track, 0, totalDuration, 0, 0, 0, "#54AB87");
    redraw();
}

function cancelAnalyserUpdates() {
    window.cancelAnimationFrame(frameID);
    frameID = null;
}

function updateAnalysers(time) {
    if (!analyserContext) {
        var canvas = document.getElementById("analyser");
        canvasWidth = canvas.width;
        canvasHeight = canvas.height;
        analyserContext = canvas.getContext('2d');
    }

    // Analyzer
    {
        var SPACING = 3;
        var BAR_WIDTH = 1;
        var numBars = Math.round(canvasWidth / SPACING);
        var freqByteData = new Uint8Array(analyserNode.frequencyBinCount);

        analyserNode.getByteFrequencyData(freqByteData); 

        analyserContext.clearRect(0, 0, canvasWidth, canvasHeight);
        analyserContext.fillStyle = '#00A9DD';
        analyserContext.lineCap = 'round';
        var multiplier = analyserNode.frequencyBinCount / numBars;

        // Draw frequencies
        for (var i = 0; i < numBars; ++i) {
            var magnitude = 0;
            var offset = Math.floor( i * multiplier );
            for (var j = 0; j< multiplier; j++)
                magnitude += freqByteData[offset + j];
            magnitude = magnitude / multiplier;
            var magnitude2 = freqByteData[i * multiplier];
            analyserContext.fillStyle = "#00A9DD";
            analyserContext.fillRect(i * SPACING, canvasHeight, BAR_WIDTH, -magnitude);
        }
    }
    
    frameID = window.requestAnimationFrame(updateAnalysers);
}

function updateTrackPlay(time) {
    if (!trackCanvas) {
        trackCanvas = document.getElementById("track");
    }

    // Player Code here
    if(sourceStartTime && (sourceStartTime + track.duration)>= audioContext.currentTime) {
        nowOffset = audioContext.currentTime - sourceStartTime;
        drawAudioBufferPart(trackCanvas.width, trackCanvas.height, trackCanvas.getContext('2d'), track, 0, totalDuration, 0, 0, nowOffset, "#54AB87");
    }

    framePID = window.requestAnimationFrame( updateTrackPlay );
}


function initAnalyzer() {
    analyserNode = audioContext.createAnalyser();
    analyserNode.fftSize = 512;
    updateAnalysers();

    updateTrackPlay();
}