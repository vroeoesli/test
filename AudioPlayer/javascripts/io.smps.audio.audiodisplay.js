/* Copyright 2014 SwissMediaPartners AG, Mathieu Habegger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

function drawAudioBufferPart(width, height, context, buffer, cue, totalduration, intro, mix, play, color) {
    var data = buffer.getChannelData( 0 );
    var duration = buffer.duration;
    var total = (data.length / duration) * totalduration;
    var cuetotal = (data.length / duration) * cue;
    var intrototal = (data.length / duration) * intro;
    var mixtotal = (data.length / duration) * mix;
    var playtotal = (data.length / duration) * play;
    var step = Math.floor( total / width );
    var cuepos = Math.floor(cuetotal / step);
    var intropos = Math.floor(intrototal / step);
    var mixpos = Math.floor((mixtotal+cuetotal) / step);
    var playpos = Math.floor((playtotal+cuetotal) / step);
    var amp = height / 2;

    context.clearRect(0,0,width,height);
    if (color) {
        if(intropos > 0) {
            context.fillStyle = "#00A9DD";
        } else {
            context.fillStyle = color;
        }
    }
    for(var i=cuepos; i < width; i++){
        if(intropos > 0 && intropos+cuepos < i) {
            context.fillStyle = color;
        }
        var min = 1.0;
        var max = -1.0;
        for (j=0; j<step; j++) {
            var datapos = ((i-cuepos)*step)+j;
            var datum = 0;
            if(datapos < data.length){
                datum = data[datapos];
            }
            if (datum < min)
                min = datum;
            if (datum > max)
                max = datum;
        }
        context.fillRect(i,(1+min)*amp,1,Math.max(1,(max-min)*amp));
    }

    // Draw Mix Point
    if(mix > 0) {
        context.fillStyle = "#00A9DD";
        context.fillRect(mixpos,0,2,2*amp);
    }
    if(play > 0) {
            context.fillStyle = "#CC4A30";
            context.fillRect(playpos,0,1,2*amp);
    }

}
